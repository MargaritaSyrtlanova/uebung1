import java.io.*;
import java.util.*;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class process_tweets {

    public static void main(String[] args){

// 	Array is created to keep all tweets from the input file
	ArrayList<tweet> table_of_tweets = new ArrayList<tweet>();
        table_of_tweets = read_tweets_from_file ("twitter.csv");
        System.out.println("Total tweets: ");
        System.out.println(table_of_tweets.size() + "\n");

// 	Array is created to keep all users from the input file (alphabetically sorted and without duplicates)
        ArrayList<String> list_of_users = new ArrayList<String>();
        list_of_users = get_list_of_users (table_of_tweets);
        System.out.println("List of Users: ");
        System.out.println("Total users: " + list_of_users.size() + "\n");

// 	Array is created to keep all hashtags from the input file (alphabetically sorted and without duplicates)
        ArrayList<String> list_of_hashtags = new ArrayList<String>();
        list_of_hashtags = get_list_of_hashtags (table_of_tweets);
        System.out.println("List of HashTags: ");
        System.out.println("Total hashtags: " + list_of_hashtags.size() + "\n");


//	User input. 4 optons to choose.
        Scanner scannerVariable = new Scanner(System.in);
        System.out.println("Press Enter");

        while(scannerVariable.nextLine() == "") {
            System.out.println("1 Get List of All Users");
            System.out.println("2 Get List of All Hashtags");
            System.out.println("3 Get List of All Tweets of Particular User");
            System.out.println("4 Search Messages");

            switch(scannerVariable.nextLine()){

                case "1":
                    for(String user : list_of_users){
                        System.out.println(user);
                    }
                    break;

                case "2":
                    for(String hashtag : list_of_hashtags){
                        System.out.println(hashtag);
                    }
                    break;

                case "3":
                    System.out.println("Enter User Name");
		    list_users_messages (scannerVariable.nextLine(), list_of_users, table_of_tweets);
                    break;

                case "4":
                    System.out.println("Search for:");
		    search_messages (scannerVariable.nextLine(), table_of_tweets);
                    break;

                case "exit":
                    System.exit(0);

                default:
                    System.out.println("Wrong Input");
            }
            System.out.println("Press Enter");
        }


    }

// The method reads input text file and fill in the array of tweets
    public static ArrayList read_tweets_from_file (String csv){

        ArrayList<tweet> Tweets_Table = new ArrayList<tweet>();

        try {
            File myObj = new File(csv);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String strTweet = myReader.nextLine();
                String[] arrTweet = strTweet.split("\\t");

                if (arrTweet.length == 7) {

                    tweet objTweet = new tweet (Long.parseLong(arrTweet[0]), Long.parseLong(arrTweet[1]), arrTweet[2], arrTweet[3], arrTweet[4], Boolean.parseBoolean(arrTweet[5]), Long.parseLong(arrTweet[6]));
                    Tweets_Table.add (objTweet);

                } else {
                    tweet objTweet = new tweet (Long.parseLong(arrTweet[0]), Long.parseLong(arrTweet[1]), arrTweet[2], arrTweet[3], arrTweet[4], Boolean.parseBoolean(arrTweet[5]), 0);
                    Tweets_Table.add (objTweet);
                }
            }
            myReader.close();
        }
	catch (FileNotFoundException e) {
		System.out.println("An error occurred.");
		e.printStackTrace();
	}

        return Tweets_Table;

    }

// The method creates array of users from array of all tweets (sorted and free from duplicates)
    public static ArrayList get_list_of_users (ArrayList<tweet> Tweets_Table){

        ArrayList<String> arrUsers = new ArrayList<String>();

        for (tweet objTweet : Tweets_Table) {
            arrUsers.add(objTweet.User);
        }

// This is to remove duplicates and sort the user's list:
        Set<String> hashUser = new LinkedHashSet<>();
        hashUser.addAll(arrUsers);
        arrUsers.clear();
        arrUsers.addAll(hashUser);
        Collections.sort(arrUsers);
        return arrUsers;

    }

// The method creates array of hashtags from array of all tweets (sorted and free from duplicates)
    public static ArrayList get_list_of_hashtags (ArrayList<tweet> Tweets_Table){

        ArrayList<String> arrHashtags = new ArrayList<String>();

        for (tweet objTweet : Tweets_Table) {

            Pattern hash_Pattern = Pattern.compile ("#(\\S+)");
            Matcher mat = hash_Pattern.matcher (objTweet.TweetMessage());

            while (mat.find()) {
                arrHashtags.add(mat.group(1));
            }
        }

// This is to remove duplicates and sort the Hashtags's list:
        Set<String> hashTags = new LinkedHashSet<>();
        hashTags.addAll(arrHashtags);
        arrHashtags.clear();
        arrHashtags.addAll(hashTags);
        Collections.sort(arrHashtags);
        return arrHashtags;
    }


// The method lists messages belonging to a user
    public static void list_users_messages (String user, ArrayList<String> arrUsers, ArrayList<tweet> Tweets_Table){

        System.out.println ("Searching messages of user " + user + ":");

        if (arrUsers.indexOf(user) < 0 ) { System.out.println ("No user found"); }
        else {
            for (tweet objTweet : Tweets_Table) {
//			if (objTweet.User == user) {
                if (objTweet.User.equals(user)) {

                    System.out.println (objTweet.User + "\t" + objTweet.TweetMessage);

                }
            }
        }

    }


// The method lists messages containing a particular string (case insensitive)
    public static void search_messages (String srcString,  ArrayList<tweet> Tweets_Table){

        System.out.println ("Searching " + srcString + " in all tweets:");

            for (tweet objTweet : Tweets_Table) {
                if (objTweet.TweetMessage.toLowerCase().indexOf(srcString.toLowerCase()) >= 0) {

			System.out.println ( objTweet.TweetMessage);

                }
            }
    }
    

}


// Class required to construct and manipulate tweet objects
class tweet{
    long 	ID, lonDate, RetweetetID;
    String 	User, Language, TweetMessage;
    boolean 	Retweeted;

    public tweet (long setID, long setDate, String setUser, String setLanguage, String setTweetMessage, boolean setRetweeted, long setRetweetetID){
        ID 		= setID;
        lonDate 	= setDate;
        User 		= setUser;
        Language 	= setLanguage;
        TweetMessage 	= setTweetMessage;
        Retweeted 	= setRetweeted;
        RetweetetID 	= setRetweetetID;
    }


    public long ID(){
        return ID;
    }

    public String User(){
        return User;
    }

    public String TweetMessage(){
        return TweetMessage;
    }

    public boolean Retweeted(){
        return Retweeted;
    }

}



