# Uebung1

In diesem Aufgabe habe ich 2 Klassen erstellt: tweet und process_tweets.
In jeder Klasse gibt es Attribute und Methoden.
Fuer tweet sind Attribute: ID, lonDate, RetweetetID, User, Language, TweetMessage, Retweeted und Methoden: ID(), User(), TweetMessage(), Retweeted().
Fuer process_tweets sind Attribute: table_of_tweets, list_of_users, list_of_hashstags und Methoden: read_tweets_from_file(), get_list_of_users(), get_list_of_hashtags(), list_users_messages().
Ich habe Menu gemacht, damit der Benutzer waehlen kann, welche Aufgabe zu fuehren.
Ich habe Kommentare line-by-line geschrieben, damit der Code ausfuehrlicher aussieht.
